.PHONY: deps-install
deps-install:
	dotnet restore

.PHONY: test
test:
	dotnet test Tests/

.PHONY: tag
tag:
	sed -i "s/0.0.0-version-placeholder/$(version)/" Events/Events.csproj

.PHONY: build
build:
	dotnet build

.PHONY: publish
publish:
	dotnet nuget push ./Events/bin/Debug/DKX.Events.$(version).nupkg -k $(api_key) -s https://api.nuget.org/v3/index.json
