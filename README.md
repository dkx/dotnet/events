# DKX/Events

Global events dispatcher

## Installation

```bash
$ dotnet add package DKX.Events
```

## Usage

**Define global event:**

```c#
using DKX.Events;

public class UserCreatedEvent : Event
{
    public UserCreatedEvent(User user)
    {
        User = user;
    }

    public User User { get; }
}
```

**Define event subscriber:**

```c#
using DKX.Events;

public class SendSlackNotificationOnUserCreated : IEventSubscriber<UserCreatedEvent>
{
    public void OnFired(UserCreatedEvent e)
    {
        var user = e.User;
        // todo: send user notification to slack
    }
}
```

**Create dispatcher in DI:**

```c#
using DKX.Events;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddScoped<IEventSubscriber, SendSlackNotificationOnUserCreated>();

        services.AddDkxEvents();
    }
}
```

It is necessary to register all subscribers with the `IEventSubscriber` type.

**Fire event:**

```c#
using DKX.Events;

public class CreateUserFacade
{
    private readonly IEventDispatcher _events;

    public CreateUserFacade(IEventDispatcher events)
    {
        _events = events;
    }

    public async Task<User> CreateUser(string email)
    {
        var user = new User(email);
        // todo: store user in database

        await _events.Fire(new UserCreatedEvent(user));
    }
}
```

## Asynchronous subscribers

Simply implement the `IEventSubscriberAsync` interface.

```c#
using DKX.Events;

public class SendSlackNotificationOnUserCreated : IEventSubscriberAsync<UserCreatedEvent>
{
    public Task OnFired(UserCreatedEvent e)
    {
        var user = e.User;
        // todo: send user notification to slack
    }
}
```

## Stop event propagation

You can call `StopPropagation()` method on given event in any subscriber. None of the following subscribers will be 
then called.
