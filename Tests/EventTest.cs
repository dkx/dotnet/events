using DKX.Events;
using Xunit;

namespace DKX.EventsTests
{
	public class EventTest
	{
		[Fact]
		public void StopPropagation()
		{
			var e = new TestEvent();

			Assert.False(e.PropagationStopped);
			e.StopPropagation();
			Assert.True(e.PropagationStopped);
		}
		
		private class TestEvent : Event {}
	}
}
