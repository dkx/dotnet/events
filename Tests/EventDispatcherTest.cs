using System.Collections.Generic;
using System.Threading.Tasks;
using DKX.Events;
using Xunit;

namespace DKX.EventsTests
{
	public class EventDispatcherTest
	{
		[Fact]
		public async Task Fire_NoSubscribers()
		{
			var dispatcher = new EventDispatcher();
			var e = new TestEventA();

			var result = await dispatcher.Fire(e);

			Assert.Same(e, result);
		}

		[Fact]
		public async Task Fire_StopPropagation()
		{
			var actions = new List<string>();
			var dispatcher = new EventDispatcher(new IEventSubscriber[]
			{
				new TestSubscriberA(actions),
				new TestSubscriberA(actions),
				new TestSubscriberAStop(actions),
				new TestSubscriberA(actions),
				new TestSubscriberA(actions),
			});

			await dispatcher.Fire(new TestEventA());

			Assert.Equal(new[]
			{
				nameof(TestSubscriberA),
				nameof(TestSubscriberA),
				nameof(TestSubscriberAStop),
			}, actions);
		}

		[Fact]
		public async Task Fire()
		{
			var actions = new List<string>();
			var dispatcher = new EventDispatcher(new IEventSubscriber[]
			{
				new TestSubscriberA(actions), 
				new TestSubscriberAAsync(actions),
				new TestSubscriberB(actions),
				new TestSubscriberBAsync(actions),
			});

			await dispatcher.Fire(new TestEventA());

			Assert.Equal(new[]
			{
				nameof(TestSubscriberA),
				nameof(TestSubscriberAAsync),
			}, actions);

			await dispatcher.Fire(new TestEventB());

			Assert.Equal(new[]
			{
				nameof(TestSubscriberA),
				nameof(TestSubscriberAAsync),
				nameof(TestSubscriberB),
				nameof(TestSubscriberBAsync),
			}, actions);
		}

		private class TestEventA : Event {}

		private class TestEventB : Event {}

		private class TestSubscriberA : IEventSubscriber<TestEventA>
		{
			private readonly IList<string> _actions;

			public TestSubscriberA(IList<string> actions)
			{
				_actions = actions;
			}

			public void OnFired(TestEventA e)
			{
				_actions.Add(nameof(TestSubscriberA));
			}
		}

		private class TestSubscriberAStop : IEventSubscriber<TestEventA>
		{
			private readonly IList<string> _actions;

			public TestSubscriberAStop(IList<string> actions)
			{
				_actions = actions;
			}

			public void OnFired(TestEventA e)
			{
				_actions.Add(nameof(TestSubscriberAStop));
				e.StopPropagation();
			}
		}

		private class TestSubscriberAAsync : IEventSubscriberAsync<TestEventA>
		{
			private readonly IList<string> _actions;

			public TestSubscriberAAsync(IList<string> actions)
			{
				_actions = actions;
			}

			public Task OnFired(TestEventA e)
			{
				_actions.Add(nameof(TestSubscriberAAsync));
				return Task.CompletedTask;
			}
		}

		private class TestSubscriberB : IEventSubscriber<TestEventB>
		{
			private readonly IList<string> _actions;

			public TestSubscriberB(IList<string> actions)
			{
				_actions = actions;
			}

			public void OnFired(TestEventB e)
			{
				_actions.Add(nameof(TestSubscriberB));
			}
		}

		private class TestSubscriberBAsync : IEventSubscriberAsync<TestEventB>
		{
			private readonly IList<string> _actions;

			public TestSubscriberBAsync(IList<string> actions)
			{
				_actions = actions;
			}

			public Task OnFired(TestEventB e)
			{
				_actions.Add(nameof(TestSubscriberBAsync));
				return Task.CompletedTask;
			}
		}
	}
}
