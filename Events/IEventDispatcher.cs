using System.Threading.Tasks;

namespace DKX.Events
{
	public interface IEventDispatcher
	{
		Task<TEvent> Fire<TEvent>(TEvent e)
			where TEvent : Event;
	}
}
