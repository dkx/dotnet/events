using System;

namespace DKX.Events
{
	public class Event : EventArgs
	{
		public bool PropagationStopped { get; private set; }

		public void StopPropagation()
		{
			PropagationStopped = true;
		}
	}
}
