using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace DKX.Events.DI
{
	public static class Extension
	{
		public static IServiceCollection AddDkxEvents(this IServiceCollection services)
		{
			services.TryAddScoped<IEventDispatcher, EventDispatcher>();

			return services;
		}
	}
}
