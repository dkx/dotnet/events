using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace DKX.Events
{
	public class EventDispatcher : IEventDispatcher
	{
		private readonly IReadOnlyCollection<IEventSubscriber> _subscribers;

		private IDictionary<Type, IList<IEventSubscriber>>? _cachedSubscribers;

		public EventDispatcher(IEnumerable<IEventSubscriber>? subscribers = null)
		{
			_subscribers = subscribers?.ToImmutableArray() ?? ImmutableArray<IEventSubscriber>.Empty;
		}

		public async Task<TEvent> Fire<TEvent>(TEvent e)
			where TEvent : Event
		{
			var eventType = typeof(TEvent);
			var subscribers = GetSubscribersForEvent(eventType);

			foreach (var subscriber in subscribers)
			{
				if (e.PropagationStopped)
				{
					break;
				}

				switch (subscriber)
				{
					case IEventSubscriber<TEvent> s: s.OnFired(e); break;
					case IEventSubscriberAsync<TEvent> s: await s.OnFired(e); break;
					default: throw new NotSupportedException($"Subscriber {subscriber.GetType()} is not supported for event {eventType}");
				}
			}

			return e;
		}

		private IEnumerable<IEventSubscriber> GetSubscribersForEvent(Type eventType)
		{
			var all = GetAllSubscribers();
			return all.ContainsKey(eventType)
				? all[eventType]
				: new IEventSubscriber[] { };
		}

		private IDictionary<Type, IList<IEventSubscriber>> GetAllSubscribers()
		{
			if (_cachedSubscribers == null)
			{
				_cachedSubscribers = new Dictionary<Type, IList<IEventSubscriber>>();

				var commonSubscriberType = typeof(IEventSubscriber);

				foreach (var subscriber in _subscribers)
				{
					var interfaceType = subscriber
						.GetType()
						.GetInterfaces()
						.FirstOrDefault(i => commonSubscriberType.IsAssignableFrom(i) && i.IsGenericType && i.GenericTypeArguments.Length == 1);

					if (interfaceType == null)
					{
						throw new NotSupportedException($"Invalid event subscriber {subscriber.GetType()}");
					}

					var eventType = interfaceType.GenericTypeArguments[0];

					if (!_cachedSubscribers.ContainsKey(eventType))
					{
						_cachedSubscribers.Add(eventType, new List<IEventSubscriber>());
					}

					_cachedSubscribers[eventType].Add(subscriber);
				}
			}

			return _cachedSubscribers;
		}
	}
}
