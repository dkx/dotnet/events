using System.Threading.Tasks;

namespace DKX.Events
{
	public interface IEventSubscriber {}

	public interface IEventSubscriber<in TEvent> : IEventSubscriber
		where TEvent : Event
	{
		void OnFired(TEvent e);
	}

	public interface IEventSubscriberAsync<in TEvent> : IEventSubscriber
		where TEvent : Event
	{
		Task OnFired(TEvent e);
	}
}
